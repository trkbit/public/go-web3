package eth_utils

import (
	"math/big"

	"github.com/ethereum/go-ethereum/params"
	decimal "github.com/shopspring/decimal"
)

func UnitGwei(unit *big.Int) *big.Int {
	return new(big.Int).Mul(unit, big.NewInt(params.GWei))
}

func ToGwei(wei *big.Int) *big.Int {
	return new(big.Int).Div(wei, big.NewInt(params.GWei))
}

func ToEther(wei *big.Int) *big.Float {
	weiFloat := new(big.Float).SetInt(wei)
	etherFloat := new(big.Float).SetInt(big.NewInt(params.Ether))

	ether := new(big.Float).Quo(weiFloat, etherFloat)

	return ether
}

func CalcGasCost(gasLimit uint64, gasPrice *big.Int) *big.Int {
	gasLimitBig := big.NewInt(int64(gasLimit))
	return gasLimitBig.Mul(gasLimitBig, gasPrice)
}

func ToWei(iamount interface{}, decimals int) *big.Int {
	amount := decimal.NewFromFloat(0)
	switch v := iamount.(type) {
	case string:
		amount, _ = decimal.NewFromString(v)
	case float64:
		amount = decimal.NewFromFloat(v)
	case int64:
		amount = decimal.NewFromFloat(float64(v))
	case decimal.Decimal:
		amount = v
	case *decimal.Decimal:
		amount = *v
	}

	mul := decimal.NewFromFloat(float64(10)).Pow(decimal.NewFromFloat(float64(decimals)))
	result := amount.Mul(mul)

	wei := new(big.Int)
	wei.SetString(result.String(), 10)

	return wei
}
