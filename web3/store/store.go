package store

import (
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
)

func NewKeystore() {
	ks := keystore.NewKeyStore("./tmp", keystore.StandardScryptN, keystore.StandardScryptP)
	password := "secret"
	account, err := ks.NewAccount(password)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(account.Address.Hex()) // 0x20F8D42FB0F667F2E53930fed426f225752453b3
}

type HDWallet struct {
	*hdwallet.Wallet
}

func ParseDerivationPath(path string) accounts.DerivationPath {
	return hdwallet.MustParseDerivationPath(path)
}

func NewHDWallet(mnemonic string) (*HDWallet, error) {
	wallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		return nil, err
	}

	return &HDWallet{wallet}, nil
}
