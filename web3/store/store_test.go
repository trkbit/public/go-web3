package store

import (
	"testing"

	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
)

func Test_Should_create_new_keystore(t *testing.T) {

}

func Test_Should_return_a_valid_ethereum_address(t *testing.T) {
	wallet, err := NewHDWallet("movie badge smooth give morning color space proud horse earth bounce kite")
	if err != nil {
		t.Error(err)
	}

	// get ethereum account
	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, err := wallet.Derive(path, true)
	if err != nil {
		t.Error(err)
	}

	if account.Address.Hex() != "0xcBa12E14f7eF7149cC61b83cE58366d660d9FC42" {
		t.Error(account.Address.Hex())
	}

	privKey, err := wallet.PrivateKeyHex(account)
	if err != nil {
		t.Error(err)
	}

	if privKey != "89e960cab8223ac91458bc163cc6ad96d173c614273147e8216f8217a35be571" {
		t.Error(privKey)
	}
}
