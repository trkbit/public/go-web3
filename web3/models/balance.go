package models

import (
	"math/big"
)

type Balance struct {
	Balance big.Int `json:"balance"`
}
