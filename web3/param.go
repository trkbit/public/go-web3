package web3

import "math/big"

const (
	RONIN_CONTRACT_SLP_ADDRESS     = "0xa8754b9fa15fc18bb59458815510e40a12cd2014"
	RONIN_CONTRACT_SCATTER_ADDRESS = "0x14978681c5f8ce2f6b66d1f1551b0ec67405574c"
)

var (
	NETWORK = map[string]NetworkParam{
		"ETHEREUM": {
			URL:      "https://mainnet.infura.io/v3/dc3a4489150b4a5f8e8fa8ddc14accf6",
			GasLimit: 21000,
			ChainId:  big.NewInt(1),
		},
		"ETHEREUM_TESTNET": {
			URL:      "https://api.roninchain.com/rpc",
			GasLimit: 21000,
		},
		"RONIN": {
			URL:      "https://api.roninchain.com/rpc",
			GasLimit: 246437, // gas price is 1 Gwei
			ChainId:  big.NewInt(2020),
		},
	}
)

type NetworkParam struct {
	URL      string
	GasLimit uint64
	ChainId  *big.Int
}
