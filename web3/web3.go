package web3

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"fmt"
	"math/big"

	"gitlab.com/trkbit/public/go-web3/eth_utils"

	abi_checkpoint "gitlab.com/trkbit/public/go-web3/web3/abi/checkpoint"
	abi_erc20 "gitlab.com/trkbit/public/go-web3/web3/abi/erc20"
	abi_erc721 "gitlab.com/trkbit/public/go-web3/web3/abi/erc721"
	abi_scatter "gitlab.com/trkbit/public/go-web3/web3/abi/scatter"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

type service struct {
	config *Config
	client *ethclient.Client
}

func (svc service) Config() Config {
	return *svc.config
}

func (svc service) Client() *ethclient.Client {
	return svc.client
}

func (svc *service) SetChainID() error {
	id, err := svc.client.ChainID(context.Background())
	if err != nil {
		return err
	}

	svc.config.Param.ChainId = id
	return nil
}

func (svc *service) Auth(privStr string) (*TransactOptsWeb3, error) {
	privKey, err := crypto.HexToECDSA(privStr)
	if err != nil {
		return nil, errors.New("failed was got priv key")
	}

	pubKey := privKey.PublicKey

	address := crypto.PubkeyToAddress(pubKey)
	nonce, err := svc.client.PendingNonceAt(context.Background(), address)
	if err != nil {
		return nil, err
	}

	auth, err := bind.NewKeyedTransactorWithChainID(privKey, svc.config.Param.ChainId)
	if err != nil {
		return nil, err
	}

	auth.Nonce = big.NewInt(int64(nonce))

	return &TransactOptsWeb3{auth}, nil
}

func (svc service) Gas() (*Gas, error) {
	gasPrice, err := svc.client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, err
	}

	gasCost := eth_utils.CalcGasCost(svc.config.Param.GasLimit, gasPrice)

	return &Gas{
		limit: svc.config.Param.GasLimit,
		price: gasPrice,
		cost:  gasCost,
	}, nil
}

func (svc service) AccountHasFee(account common.Address) error {
	balance, err := svc.client.BalanceAt(context.Background(), account, nil)
	if err != nil {
		return err
	}

	gas, err := svc.Gas()
	if err != nil {
		return err
	}

	if balance.Cmp(gas.Cost()) == -1 {
		return fmt.Errorf("address has insuficient amount to gas fee which cost %s", gas.EtherCost().String())
	}

	return nil
}

func (svc service) Balance(account common.Address) (*big.Int, error) {
	balance, err := svc.client.BalanceAt(context.Background(), account, nil)
	if err != nil {
		return nil, err
	}

	return balance, nil
}

func (svc service) GetTransferByHash(hash common.Hash) (*types.Transaction, bool, error) {
	tx, pending, err := svc.client.TransactionByHash(context.Background(), hash)
	if err != nil {
		return nil, false, fmt.Errorf("failed to get transcation by hash %s - %s", hash.String(), err.Error())
	}

	return tx, pending, err
}

func (svc service) SendTransaction(tx *types.Transaction, privkey *ecdsa.PrivateKey) error {
	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(svc.config.Param.ChainId), privkey)
	if err != nil {
		return err
	}

	err = svc.client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		return err
	}

	return err
}

func (svc service) LoadErc20(addr common.Address) (*abi_erc20.AbiErc20, error) {
	instance, err := abi_erc20.NewAbiErc20(addr, svc.client)
	if err != nil {
		return nil, fmt.Errorf("failed to instance ERC20 contract from address %s - %s", addr.Hex(), err.Error())
	}

	return instance, nil
}

func (svc service) LoadErc721(addr common.Address) (*abi_erc721.ERC721, error) {
	instance, err := abi_erc721.NewERC721(addr, svc.client)
	if err != nil {
		return nil, fmt.Errorf("failed to instance ERC721 contract from address %s - %s", addr.Hex(), err.Error())
	}

	return instance, nil
}

func (svc service) LoadCheckpoint(contract common.Address) (*abi_checkpoint.AbiCheckpoint, error) {
	if svc.config.NetworkName != "RONIN" {
		return nil, fmt.Errorf("network does not have support to load this contract.")
	}

	instance, err := abi_checkpoint.NewAbiCheckpoint(contract, svc.client)
	if err != nil {
		return nil, fmt.Errorf("failed to instance checkpoint contract from address %s - %s", contract.Hex(), err.Error())
	}

	return instance, nil
}

func (svc service) LoadScatter() (*abi_scatter.AbiScatter, error) {
	addr := common.HexToAddress(RONIN_CONTRACT_SCATTER_ADDRESS)

	if svc.config.NetworkName != "RONIN" {
		return nil, fmt.Errorf("network does not have support to load this contract.")
	}

	instance, err := abi_scatter.NewAbiScatter(addr, svc.client)
	if err != nil {
		return nil, fmt.Errorf("failed to instance scatter contract from address %s - %s", addr.Hex(), err.Error())
	}

	return instance, nil
}

func (svc service) Close() {
	svc.client.Close()
}

func NewTransaction(nonce uint64, toAddress common.Address, value *big.Int, data []byte, gas *Gas) *types.Transaction {
	return types.NewTransaction(nonce, toAddress, value, gas.Limit(), gas.Price(), data)
}

type TransactOptsWeb3 struct {
	*bind.TransactOpts
}

func (t TransactOptsWeb3) ToTransactOpts() *bind.TransactOpts {
	return t.TransactOpts
}
