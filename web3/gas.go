package web3

import (
	"math/big"

	"gitlab.com/trkbit/public/go-web3/eth_utils"
)

type Gas struct {
	limit uint64
	price *big.Int // wei
	cost  *big.Int // wei
}

func (g Gas) Limit() uint64 {
	return g.limit
}

func (g Gas) Price() *big.Int {
	return g.price
}

func (g Gas) Cost() *big.Int {
	return g.cost
}

func (g Gas) EtherCost() *big.Float {
	return eth_utils.ToEther(g.cost)
}
