// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abi_approve

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AbiApproveMetaData contains all meta data concerning the AbiApprove contract.
var AbiApproveMetaData = &bind.MetaData{
	ABI: "[{\"constant\":false,\"inputs\":[{\"name\":\"_spender\",\"type\":\"address\"},{\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"_owner\",\"type\":\"address\"},{\"name\":\"_spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// AbiApproveABI is the input ABI used to generate the binding from.
// Deprecated: Use AbiApproveMetaData.ABI instead.
var AbiApproveABI = AbiApproveMetaData.ABI

// AbiApprove is an auto generated Go binding around an Ethereum contract.
type AbiApprove struct {
	AbiApproveCaller     // Read-only binding to the contract
	AbiApproveTransactor // Write-only binding to the contract
	AbiApproveFilterer   // Log filterer for contract events
}

// AbiApproveCaller is an auto generated read-only Go binding around an Ethereum contract.
type AbiApproveCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiApproveTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AbiApproveTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiApproveFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AbiApproveFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiApproveSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AbiApproveSession struct {
	Contract     *AbiApprove       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AbiApproveCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AbiApproveCallerSession struct {
	Contract *AbiApproveCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// AbiApproveTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AbiApproveTransactorSession struct {
	Contract     *AbiApproveTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// AbiApproveRaw is an auto generated low-level Go binding around an Ethereum contract.
type AbiApproveRaw struct {
	Contract *AbiApprove // Generic contract binding to access the raw methods on
}

// AbiApproveCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AbiApproveCallerRaw struct {
	Contract *AbiApproveCaller // Generic read-only contract binding to access the raw methods on
}

// AbiApproveTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AbiApproveTransactorRaw struct {
	Contract *AbiApproveTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAbiApprove creates a new instance of AbiApprove, bound to a specific deployed contract.
func NewAbiApprove(address common.Address, backend bind.ContractBackend) (*AbiApprove, error) {
	contract, err := bindAbiApprove(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &AbiApprove{AbiApproveCaller: AbiApproveCaller{contract: contract}, AbiApproveTransactor: AbiApproveTransactor{contract: contract}, AbiApproveFilterer: AbiApproveFilterer{contract: contract}}, nil
}

// NewAbiApproveCaller creates a new read-only instance of AbiApprove, bound to a specific deployed contract.
func NewAbiApproveCaller(address common.Address, caller bind.ContractCaller) (*AbiApproveCaller, error) {
	contract, err := bindAbiApprove(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AbiApproveCaller{contract: contract}, nil
}

// NewAbiApproveTransactor creates a new write-only instance of AbiApprove, bound to a specific deployed contract.
func NewAbiApproveTransactor(address common.Address, transactor bind.ContractTransactor) (*AbiApproveTransactor, error) {
	contract, err := bindAbiApprove(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AbiApproveTransactor{contract: contract}, nil
}

// NewAbiApproveFilterer creates a new log filterer instance of AbiApprove, bound to a specific deployed contract.
func NewAbiApproveFilterer(address common.Address, filterer bind.ContractFilterer) (*AbiApproveFilterer, error) {
	contract, err := bindAbiApprove(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AbiApproveFilterer{contract: contract}, nil
}

// bindAbiApprove binds a generic wrapper to an already deployed contract.
func bindAbiApprove(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AbiApproveABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiApprove *AbiApproveRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiApprove.Contract.AbiApproveCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiApprove *AbiApproveRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiApprove.Contract.AbiApproveTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiApprove *AbiApproveRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiApprove.Contract.AbiApproveTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiApprove *AbiApproveCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiApprove.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiApprove *AbiApproveTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiApprove.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiApprove *AbiApproveTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiApprove.Contract.contract.Transact(opts, method, params...)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address _owner, address _spender) view returns(uint256)
func (_AbiApprove *AbiApproveCaller) Allowance(opts *bind.CallOpts, _owner common.Address, _spender common.Address) (*big.Int, error) {
	var out []interface{}
	err := _AbiApprove.contract.Call(opts, &out, "allowance", _owner, _spender)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address _owner, address _spender) view returns(uint256)
func (_AbiApprove *AbiApproveSession) Allowance(_owner common.Address, _spender common.Address) (*big.Int, error) {
	return _AbiApprove.Contract.Allowance(&_AbiApprove.CallOpts, _owner, _spender)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address _owner, address _spender) view returns(uint256)
func (_AbiApprove *AbiApproveCallerSession) Allowance(_owner common.Address, _spender common.Address) (*big.Int, error) {
	return _AbiApprove.Contract.Allowance(&_AbiApprove.CallOpts, _owner, _spender)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _spender, uint256 _value) returns()
func (_AbiApprove *AbiApproveTransactor) Approve(opts *bind.TransactOpts, _spender common.Address, _value *big.Int) (*types.Transaction, error) {
	return _AbiApprove.contract.Transact(opts, "approve", _spender, _value)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _spender, uint256 _value) returns()
func (_AbiApprove *AbiApproveSession) Approve(_spender common.Address, _value *big.Int) (*types.Transaction, error) {
	return _AbiApprove.Contract.Approve(&_AbiApprove.TransactOpts, _spender, _value)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address _spender, uint256 _value) returns()
func (_AbiApprove *AbiApproveTransactorSession) Approve(_spender common.Address, _value *big.Int) (*types.Transaction, error) {
	return _AbiApprove.Contract.Approve(&_AbiApprove.TransactOpts, _spender, _value)
}
