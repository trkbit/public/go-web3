// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abi_checkpoint

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AbiCheckpointMetaData contains all meta data concerning the AbiCheckpoint contract.
var AbiCheckpointMetaData = &bind.MetaData{
	ABI: "[{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_createdAt\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_signature\",\"type\":\"bytes\"}],\"name\":\"checkpoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"_balance\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// AbiCheckpointABI is the input ABI used to generate the binding from.
// Deprecated: Use AbiCheckpointMetaData.ABI instead.
var AbiCheckpointABI = AbiCheckpointMetaData.ABI

// AbiCheckpoint is an auto generated Go binding around an Ethereum contract.
type AbiCheckpoint struct {
	AbiCheckpointCaller     // Read-only binding to the contract
	AbiCheckpointTransactor // Write-only binding to the contract
	AbiCheckpointFilterer   // Log filterer for contract events
}

// AbiCheckpointCaller is an auto generated read-only Go binding around an Ethereum contract.
type AbiCheckpointCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiCheckpointTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AbiCheckpointTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiCheckpointFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AbiCheckpointFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiCheckpointSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AbiCheckpointSession struct {
	Contract     *AbiCheckpoint    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AbiCheckpointCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AbiCheckpointCallerSession struct {
	Contract *AbiCheckpointCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// AbiCheckpointTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AbiCheckpointTransactorSession struct {
	Contract     *AbiCheckpointTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// AbiCheckpointRaw is an auto generated low-level Go binding around an Ethereum contract.
type AbiCheckpointRaw struct {
	Contract *AbiCheckpoint // Generic contract binding to access the raw methods on
}

// AbiCheckpointCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AbiCheckpointCallerRaw struct {
	Contract *AbiCheckpointCaller // Generic read-only contract binding to access the raw methods on
}

// AbiCheckpointTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AbiCheckpointTransactorRaw struct {
	Contract *AbiCheckpointTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAbiCheckpoint creates a new instance of AbiCheckpoint, bound to a specific deployed contract.
func NewAbiCheckpoint(address common.Address, backend bind.ContractBackend) (*AbiCheckpoint, error) {
	contract, err := bindAbiCheckpoint(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &AbiCheckpoint{AbiCheckpointCaller: AbiCheckpointCaller{contract: contract}, AbiCheckpointTransactor: AbiCheckpointTransactor{contract: contract}, AbiCheckpointFilterer: AbiCheckpointFilterer{contract: contract}}, nil
}

// NewAbiCheckpointCaller creates a new read-only instance of AbiCheckpoint, bound to a specific deployed contract.
func NewAbiCheckpointCaller(address common.Address, caller bind.ContractCaller) (*AbiCheckpointCaller, error) {
	contract, err := bindAbiCheckpoint(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AbiCheckpointCaller{contract: contract}, nil
}

// NewAbiCheckpointTransactor creates a new write-only instance of AbiCheckpoint, bound to a specific deployed contract.
func NewAbiCheckpointTransactor(address common.Address, transactor bind.ContractTransactor) (*AbiCheckpointTransactor, error) {
	contract, err := bindAbiCheckpoint(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AbiCheckpointTransactor{contract: contract}, nil
}

// NewAbiCheckpointFilterer creates a new log filterer instance of AbiCheckpoint, bound to a specific deployed contract.
func NewAbiCheckpointFilterer(address common.Address, filterer bind.ContractFilterer) (*AbiCheckpointFilterer, error) {
	contract, err := bindAbiCheckpoint(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AbiCheckpointFilterer{contract: contract}, nil
}

// bindAbiCheckpoint binds a generic wrapper to an already deployed contract.
func bindAbiCheckpoint(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AbiCheckpointABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiCheckpoint *AbiCheckpointRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiCheckpoint.Contract.AbiCheckpointCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiCheckpoint *AbiCheckpointRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.AbiCheckpointTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiCheckpoint *AbiCheckpointRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.AbiCheckpointTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiCheckpoint *AbiCheckpointCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiCheckpoint.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiCheckpoint *AbiCheckpointTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiCheckpoint *AbiCheckpointTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.contract.Transact(opts, method, params...)
}

// Checkpoint is a paid mutator transaction binding the contract method 0xd3392ddf.
//
// Solidity: function checkpoint(address _owner, uint256 _amount, uint256 _createdAt, bytes _signature) returns(uint256 _balance)
func (_AbiCheckpoint *AbiCheckpointTransactor) Checkpoint(opts *bind.TransactOpts, _owner common.Address, _amount *big.Int, _createdAt *big.Int, _signature []byte) (*types.Transaction, error) {
	return _AbiCheckpoint.contract.Transact(opts, "checkpoint", _owner, _amount, _createdAt, _signature)
}

// Checkpoint is a paid mutator transaction binding the contract method 0xd3392ddf.
//
// Solidity: function checkpoint(address _owner, uint256 _amount, uint256 _createdAt, bytes _signature) returns(uint256 _balance)
func (_AbiCheckpoint *AbiCheckpointSession) Checkpoint(_owner common.Address, _amount *big.Int, _createdAt *big.Int, _signature []byte) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.Checkpoint(&_AbiCheckpoint.TransactOpts, _owner, _amount, _createdAt, _signature)
}

// Checkpoint is a paid mutator transaction binding the contract method 0xd3392ddf.
//
// Solidity: function checkpoint(address _owner, uint256 _amount, uint256 _createdAt, bytes _signature) returns(uint256 _balance)
func (_AbiCheckpoint *AbiCheckpointTransactorSession) Checkpoint(_owner common.Address, _amount *big.Int, _createdAt *big.Int, _signature []byte) (*types.Transaction, error) {
	return _AbiCheckpoint.Contract.Checkpoint(&_AbiCheckpoint.TransactOpts, _owner, _amount, _createdAt, _signature)
}
