// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abi_scatter

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AbiScatterMetaData contains all meta data concerning the AbiScatter contract.
var AbiScatterMetaData = &bind.MetaData{
	ABI: "[{\"constant\":false,\"inputs\":[{\"name\":\"token\",\"type\":\"address\"},{\"name\":\"recipients\",\"type\":\"address[]\"},{\"name\":\"values\",\"type\":\"uint256[]\"}],\"name\":\"disperseTokenSimple\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"token\",\"type\":\"address\"},{\"name\":\"recipients\",\"type\":\"address[]\"},{\"name\":\"values\",\"type\":\"uint256[]\"}],\"name\":\"disperseToken\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"recipients\",\"type\":\"address[]\"},{\"name\":\"values\",\"type\":\"uint256[]\"}],\"name\":\"disperseEther\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"}]",
}

// AbiScatterABI is the input ABI used to generate the binding from.
// Deprecated: Use AbiScatterMetaData.ABI instead.
var AbiScatterABI = AbiScatterMetaData.ABI

// AbiScatter is an auto generated Go binding around an Ethereum contract.
type AbiScatter struct {
	AbiScatterCaller     // Read-only binding to the contract
	AbiScatterTransactor // Write-only binding to the contract
	AbiScatterFilterer   // Log filterer for contract events
}

// AbiScatterCaller is an auto generated read-only Go binding around an Ethereum contract.
type AbiScatterCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiScatterTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AbiScatterTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiScatterFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AbiScatterFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AbiScatterSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AbiScatterSession struct {
	Contract     *AbiScatter       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AbiScatterCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AbiScatterCallerSession struct {
	Contract *AbiScatterCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// AbiScatterTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AbiScatterTransactorSession struct {
	Contract     *AbiScatterTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// AbiScatterRaw is an auto generated low-level Go binding around an Ethereum contract.
type AbiScatterRaw struct {
	Contract *AbiScatter // Generic contract binding to access the raw methods on
}

// AbiScatterCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AbiScatterCallerRaw struct {
	Contract *AbiScatterCaller // Generic read-only contract binding to access the raw methods on
}

// AbiScatterTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AbiScatterTransactorRaw struct {
	Contract *AbiScatterTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAbiScatter creates a new instance of AbiScatter, bound to a specific deployed contract.
func NewAbiScatter(address common.Address, backend bind.ContractBackend) (*AbiScatter, error) {
	contract, err := bindAbiScatter(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &AbiScatter{AbiScatterCaller: AbiScatterCaller{contract: contract}, AbiScatterTransactor: AbiScatterTransactor{contract: contract}, AbiScatterFilterer: AbiScatterFilterer{contract: contract}}, nil
}

// NewAbiScatterCaller creates a new read-only instance of AbiScatter, bound to a specific deployed contract.
func NewAbiScatterCaller(address common.Address, caller bind.ContractCaller) (*AbiScatterCaller, error) {
	contract, err := bindAbiScatter(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AbiScatterCaller{contract: contract}, nil
}

// NewAbiScatterTransactor creates a new write-only instance of AbiScatter, bound to a specific deployed contract.
func NewAbiScatterTransactor(address common.Address, transactor bind.ContractTransactor) (*AbiScatterTransactor, error) {
	contract, err := bindAbiScatter(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AbiScatterTransactor{contract: contract}, nil
}

// NewAbiScatterFilterer creates a new log filterer instance of AbiScatter, bound to a specific deployed contract.
func NewAbiScatterFilterer(address common.Address, filterer bind.ContractFilterer) (*AbiScatterFilterer, error) {
	contract, err := bindAbiScatter(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AbiScatterFilterer{contract: contract}, nil
}

// bindAbiScatter binds a generic wrapper to an already deployed contract.
func bindAbiScatter(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AbiScatterABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiScatter *AbiScatterRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiScatter.Contract.AbiScatterCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiScatter *AbiScatterRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiScatter.Contract.AbiScatterTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiScatter *AbiScatterRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiScatter.Contract.AbiScatterTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AbiScatter *AbiScatterCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AbiScatter.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AbiScatter *AbiScatterTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AbiScatter.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AbiScatter *AbiScatterTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AbiScatter.Contract.contract.Transact(opts, method, params...)
}

// DisperseEther is a paid mutator transaction binding the contract method 0xe63d38ed.
//
// Solidity: function disperseEther(address[] recipients, uint256[] values) payable returns()
func (_AbiScatter *AbiScatterTransactor) DisperseEther(opts *bind.TransactOpts, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.contract.Transact(opts, "disperseEther", recipients, values)
}

// DisperseEther is a paid mutator transaction binding the contract method 0xe63d38ed.
//
// Solidity: function disperseEther(address[] recipients, uint256[] values) payable returns()
func (_AbiScatter *AbiScatterSession) DisperseEther(recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseEther(&_AbiScatter.TransactOpts, recipients, values)
}

// DisperseEther is a paid mutator transaction binding the contract method 0xe63d38ed.
//
// Solidity: function disperseEther(address[] recipients, uint256[] values) payable returns()
func (_AbiScatter *AbiScatterTransactorSession) DisperseEther(recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseEther(&_AbiScatter.TransactOpts, recipients, values)
}

// DisperseToken is a paid mutator transaction binding the contract method 0xc73a2d60.
//
// Solidity: function disperseToken(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterTransactor) DisperseToken(opts *bind.TransactOpts, token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.contract.Transact(opts, "disperseToken", token, recipients, values)
}

// DisperseToken is a paid mutator transaction binding the contract method 0xc73a2d60.
//
// Solidity: function disperseToken(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterSession) DisperseToken(token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseToken(&_AbiScatter.TransactOpts, token, recipients, values)
}

// DisperseToken is a paid mutator transaction binding the contract method 0xc73a2d60.
//
// Solidity: function disperseToken(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterTransactorSession) DisperseToken(token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseToken(&_AbiScatter.TransactOpts, token, recipients, values)
}

// DisperseTokenSimple is a paid mutator transaction binding the contract method 0x51ba162c.
//
// Solidity: function disperseTokenSimple(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterTransactor) DisperseTokenSimple(opts *bind.TransactOpts, token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.contract.Transact(opts, "disperseTokenSimple", token, recipients, values)
}

// DisperseTokenSimple is a paid mutator transaction binding the contract method 0x51ba162c.
//
// Solidity: function disperseTokenSimple(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterSession) DisperseTokenSimple(token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseTokenSimple(&_AbiScatter.TransactOpts, token, recipients, values)
}

// DisperseTokenSimple is a paid mutator transaction binding the contract method 0x51ba162c.
//
// Solidity: function disperseTokenSimple(address token, address[] recipients, uint256[] values) returns()
func (_AbiScatter *AbiScatterTransactorSession) DisperseTokenSimple(token common.Address, recipients []common.Address, values []*big.Int) (*types.Transaction, error) {
	return _AbiScatter.Contract.DisperseTokenSimple(&_AbiScatter.TransactOpts, token, recipients, values)
}
