package web3

import (
	"crypto/ecdsa"
	"math/big"

	abi_checkpoint "gitlab.com/trkbit/public/go-web3/web3/abi/checkpoint"
	abi_erc20 "gitlab.com/trkbit/public/go-web3/web3/abi/erc20"
	abi_erc721 "gitlab.com/trkbit/public/go-web3/web3/abi/erc721"
	abi_scatter "gitlab.com/trkbit/public/go-web3/web3/abi/scatter"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

type Web3 interface {
	Client() *ethclient.Client

	Config() Config

	SetChainID() error
	Auth(privStr string) (*TransactOptsWeb3, error)
	Gas() (*Gas, error)
	AccountHasFee(account common.Address) error
	Balance(account common.Address) (*big.Int, error)
	SendTransaction(tx *types.Transaction, privkey *ecdsa.PrivateKey) error
	GetTransferByHash(hash common.Hash) (*types.Transaction, bool, error)

	LoadErc20(addr common.Address) (*abi_erc20.AbiErc20, error)
	LoadErc721(addr common.Address) (*abi_erc721.ERC721, error)
	LoadCheckpoint(contract common.Address) (*abi_checkpoint.AbiCheckpoint, error)
	LoadScatter() (*abi_scatter.AbiScatter, error)

	Close()
}

func New(config *Config) (Web3, error) {
	client, err := ethclient.Dial(config.Param.URL)
	if err != nil {
		return nil, err
	}

	return &service{
		client: client,
		config: config,
	}, nil
}

type Config struct {
	NetworkName string
	Param       NetworkParam
}

func NewConfig(networkName string) *Config {
	param := NETWORK[networkName]

	return &Config{
		NetworkName: networkName,
		Param:       param,
	}
}
